from argparse import ArgumentParser


def parse_arguments():
    parser = ArgumentParser("Run CNN on Given Spectra and Concentration Data")
    parser._action_groups.pop()

    general = parser.add_argument_group("general")
    focus = parser.add_argument_group("focus")
    training = parser.add_argument_group("training")
    prediction = parser.add_argument_group("prediction")

    general.add_argument(
        "-i",
        "--ion",
        type=str,
        default="lithium",
        choices=["lithium", "tegdme"],
        help="The electrolyte system to model.",
    )
    general.add_argument(
        "--restrict",
        type=int,
        default=None,
        choices=[None, 7, 10, 15, 20, 30, 50, 100, 250, 500, 750, 1000],
        help="Set the maximum number of samples to train on.",
    )

    focus.add_argument(
        "-f",
        "--focus",
        action="store_true",
        help="Train on a dataset focused on wavenumber or molarity.",
    )
    focus.add_argument(
        "--molarity",
        action="store_true",
        help="Create and train on a focused dataset on molarity.",
    )
    focus.add_argument(
        "--wavenumber",
        action="store_true",
        help="Create and train on a focused dataset on wavenumber.",
    )
    focus.add_argument(
        "--min",
        type=float,
        default=0.0,
        help="Set the range minimum to focus the dataset on.",
    )
    focus.add_argument(
        "--max",
        type=float,
        default=0.0,
        help="Set the range maximum to focus the dataset on.",
    )

    training.add_argument(
        "-t",
        "--train",
        action="store_true",
        help="Train a new model on a dataset.",
    )
    training.add_argument(
        "-m",
        "--measure",
        type=str,
        default="interpolated",
        choices=["interpolated", "measured"],
        help="Train the model using the measured or interpolated dataset.",
    )
    training.add_argument(
        "-s",
        "--split",
        type=str,
        default="30-70",
        choices=["0-100", "20-80", "30-70", "50-50", "70-30", "80-20"],
        help="The ratios of EC and EMC in the dataset. This only applies to "
        "lithium.",
    )

    prediction.add_argument(
        "-p",
        "--predict",
        action="store_true",
        help="Generate model predictions with a trained model on a battery "
        "dataset.",
    )
    prediction.add_argument(
        "-l",
        "--latest",
        action="store_true",
        help="Run model predictions with the model from the latest training "
        "run.",
    )
    prediction.add_argument(
        "-c",
        "--corrected",
        action="store_true",
        help="Run model predictions on the battery data corrected for baseline "
        "shifting.",
    )
    prediction.add_argument(
        "-b",
        "--battery",
        type=str,
        default="all",
        choices=["1.00", "2.33", "3.00", "all"],
        help="The charging rate for the battery cycling data for model "
        "predictions.",
    )
    prediction.add_argument(
        "-r",
        "--results-path",
        type=str,
        default=None,
        help="Path to run results folder for generating model predictions. "
        "This is required if --latest is not supplied.",
    )

    args = parser.parse_args()
    print(f"\nArguments: {args}\n")

    return args
