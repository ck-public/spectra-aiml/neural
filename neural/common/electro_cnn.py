import json

from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.initializers import TruncatedNormal
from keras.layers import Conv1D, Dense, Dropout, Flatten, MaxPooling1D
from keras.models import Sequential
from sklearn.base import BaseEstimator, RegressorMixin


class ElectroCNN(BaseEstimator, RegressorMixin):
    """A class for training a tensorflow model and generating predictions."""

    def __init__(
        self,
        spectra_length,
        results_path,
        conv_filters=4,
        conv_kernel_size=9,
        add_maxpool=False,
        dense_filters=32,
        dense_layers=2,
        dense_activation="elu",
        output_filters=3,
        output_activation="softmax",
        epochs=100,
        batch_size=10,
        dropout=0.25,
        validation_split=0.1,
    ):
        """Initializes an ElectroCNN class.

        Args:
            spectra_length: The length an input spectra sample to the model.
            results_path: The path to save the model configuration.
            conv_filters: The number of filters in the convolutional layers.
            conv_kernel_size: The size of the convolutional kernel.
            add_maxpool: Whether to add maxpooling to convolutional output.
            dense_filters: The number of filters in each dense layer.
            dense_layers: The number of hidden dense layers in the model.
            dense_activation: The activation function applied to dense layers.
            output_filters: The number of outputs you would like to have.
            output_activation: The activation of the output layer.
            epochs: The number of epochs to train the model for.
            batch_size: The number of spectra to include ina batch of data.
            dropout: The percent of data to drop from the input spectra samples.
            validation_split: The percent of training data to use for validating
                the model.
        """
        # Parameters:
        self.spectra_length = spectra_length
        self.results_path = results_path

        # Convolutional Layers:
        self.conv_filters = conv_filters
        self.conv_kernel_size = conv_kernel_size
        self.add_maxpool = add_maxpool

        # Dense Layers:
        self.dense_filters = dense_filters
        self.dense_layers = dense_layers
        self.activation = dense_activation

        # Output Layer:
        self.output_filters = output_filters
        self.output_activation = output_activation

        # Training Parameters:
        self.epochs = epochs
        self.batch_size = batch_size
        self.dropout = dropout
        self.validation_split = validation_split

        # Model and Checkpoints:
        self.optimizer = "adam"
        self.model = Sequential()
        self.initializer = TruncatedNormal(stddev=0.01)
        self.best_model_file = self.results_path / "best_model.hdf5"
        self.early_stopper = EarlyStopping(
            monitor="val_loss",
            mode="min",
            patience=10,
            verbose=1,
            restore_best_weights=True,
        )
        self.checkpoint = ModelCheckpoint(
            self.best_model_file,
            monitor="val_loss",
            mode="min",
            verbose=1,
            save_best_only=True,
        )
        self.lr_reducer = ReduceLROnPlateau(
            monitor="val_loss", mode="min", patience=7, verbose=1
        )

        # Write Configuration:
        self.configuration_file = self.results_path / "model_configuration.json"
        self.write_configuration()

        self.build()

    def write_configuration(self):
        configuration = {
            "spectra_length": self.spectra_length,
            "dense_activation": self.activation,
            "conv_filters": self.conv_filters,
            "conv_kernel_size": self.conv_kernel_size,
            "dense_filters": self.dense_filters,
            "dense_layers": self.dense_layers,
            "output_filters": self.output_filters,
            "output_activation": self.output_activation,
            "epochs": self.epochs,
            "batch_size": self.batch_size,
            "dropout": self.dropout,
            "add_maxpool": self.add_maxpool,
            "validation_split": self.validation_split,
            "optimizer": self.optimizer,
        }
        with open(self.configuration_file, "w+") as wf:
            wf.write(json.dumps(configuration, indent=4))

    def build(self):
        # Construct the Network:
        print("\n***** Constructing the Spectra NN Model *****\n")

        # Input Shape:
        print("\nDefining the Input Shape\n")
        input_shape = (self.spectra_length, 1)
        print(f"------> Input Shape: {input_shape}\n")

        # Input Layer:
        print("\nBuilding the Input Layer\n")
        print(
            f"---> Dropout (0.25) Input Layer with Conv1D ({self.conv_filters} "
            "Nodes) Layer"
        )
        self.model.add(Dropout(self.dropout, input_shape=input_shape))
        self.model.add(
            Conv1D(
                filters=self.conv_filters,
                activation=self.activation,
                kernel_size=self.conv_kernel_size,
                kernel_initializer=self.initializer,
            )
        )

        # Max Pooling:
        if self.add_maxpool:
            self.model.add(MaxPooling1D(pool_size=2))

        # Middle Layers:
        print("\nBuilding the Middle Layers\n")
        print("---> Adding Flatten Layer")
        self.model.add(Flatten())

        for i in range(self.dense_layers):
            print(
                f"---> Adding Dense Layer {i + 1} ({self.dense_filters} Nodes) "
                "with {self.activation.upper()} Activation"
            )
            self.model.add(
                Dense(
                    units=self.dense_filters,
                    activation=self.activation,
                    kernel_initializer=self.initializer,
                    name=f"dense_{i + 1}",
                )
            )

        # Output Layer:
        # Lithium Molarity: 1 Output Node with Linear Activation
        # Li, EC, and EMC Mole Fractions: 3 Output Nodes with Sigmoid Activation
        print("\nBuilding the Output Layer\n")
        print(
            f"---> Adding Dense Layer ({self.output_filters} Nodes) with "
            f"{self.output_activation.upper()} Activation"
        )
        self.model.add(
            Dense(
                self.output_filters,
                activation=self.output_activation,
                kernel_initializer=self.initializer,
                name="dense_output",
            )
        )

        # Compile the Model:
        print(
            f"Compiling the Model (Loss: MSR) (Optimizer: {self.optimizer})\n"
        )
        self.model.compile(loss="mean_squared_error", optimizer=self.optimizer)

    def fit(self, x, y):
        # Fitting the Model:
        print(
            f"Fitting the Model (Batch Size: {self.batch_size}) (Epochs: "
            f"{self.epochs})\n"
        )
        self.history = self.model.fit(
            x,
            y,
            validation_split=self.validation_split,
            batch_size=self.batch_size,
            epochs=self.epochs,
            verbose=1,
            callbacks=[self.early_stopper, self.checkpoint, self.lr_reducer],
        )

    def predict(self, x):
        return self.model.predict(x)
