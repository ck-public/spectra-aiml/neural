# Standard:
import subprocess

import matplotlib.pyplot as plt

# External:
import numpy as np
import pandas as pd

# Globals:
REPO_PATH = (
    subprocess.check_output("git rev-parse --show-toplevel", shell=True)
    .decode()
    .replace("\n", "")
)
DATA_PATH = f"{REPO_PATH}/../data"

# Plotting Globals:
DPI = 80
FIGURE_SIZE = (50, 30)
TITLE_PLOT_KWARGS = {"fontsize": 90, "fontweight": "bold"}
LABEL_PLOT_KWARGS = {"fontsize": 80, "fontweight": "bold"}
LEGEND_PLOT_KWARGS = {"fontsize": 80}
MARKER_SIZE = 1000
SMALLER_MARKER_SIZE = 800


# Plot Battery Cycling Data Spectra:
def plot_battery_data(rate, spectra, spectra_mod, wavenumbers, results_path):
    print("\nPlotting the Battery Cycling Spectra\n")
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    plt.subplot(2, 1, 1)
    for s in spectra:
        plt.plot(wavenumbers, s)
    plt.title("Original Spectra", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(2, 1, 2)
    for s in spectra_mod:
        plt.plot(wavenumbers, s)
    plt.title("Scaled Spectra", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / f"battery_spectra_{rate}.png")
    plt.clf()


# Plot Training Data Spectra:
def plot_data(
    spectra,
    spectra_mod,
    scaler_spectra,
    x_train,
    x_test,
    wavenumbers,
    results_path,
):
    # Plot All Spectra (Train and Test):
    print("\nPlotting the Spectra\n")
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    plt.subplot(2, 1, 1)
    for s in spectra:
        plt.plot(wavenumbers, s)
    plt.title("All Spectra (Original)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(2, 1, 2)
    for s in spectra_mod:
        plt.plot(wavenumbers, s)
    plt.title("All Spectra (Scaled)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / "spectra_all.png")
    plt.clf()

    # Plot Training Spectra Only:
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    x_train_original = scaler_spectra.inverse_transform(x_train.squeeze())
    plt.subplot(2, 1, 1)
    for s in x_train_original:
        plt.plot(wavenumbers, s)
    plt.title("Training Spectra (Original)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(2, 1, 2)
    for s in x_train:
        plt.plot(wavenumbers, s)
    plt.title("Training Spectra (Spectra)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / "spectra_train.png")
    plt.clf()

    # Plot Testing Spectra Only:
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    x_test_original = scaler_spectra.inverse_transform(x_test.squeeze())
    plt.subplot(2, 1, 1)
    for s in x_test_original:
        plt.plot(wavenumbers, s)
    plt.title("Testing Spectra (Original)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(2, 1, 2)
    for s in x_test:
        plt.plot(wavenumbers, s)
    plt.title("Testing Spectra (Scaled)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / "spectra_test.png")
    plt.clf()


# Plot True versus Predicted Molarities:
def plot_true_vs_predicted(y_test_real, y_pred_real, score, results_path):
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)

    plt.subplot(2, 1, 1)
    plt.scatter(
        y_test_real,
        y_pred_real,
        s=MARKER_SIZE,
        facecolors="m",
        edgecolors="m",
        linewidths=4,
        label="True",
    )
    plt.plot(y_test_real, y_test_real, c="k", label="Pred")
    plt.annotate(
        f"Score: {round(score, 6)}",
        xy=(0.60, 0.20),
        xycoords="axes fraction",
        fontsize=LABEL_PLOT_KWARGS["fontsize"],
    )
    plt.title("Model Predictions vs True Molarities", **TITLE_PLOT_KWARGS)
    plt.xlabel("True Molarity [M]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Pred Molarity [M]", **LABEL_PLOT_KWARGS)
    plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.legend(**LEGEND_PLOT_KWARGS)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    # Plot the Error Residuals for Predictions:
    errors = np.abs((y_test_real - y_pred_real) / y_test_real) * 100
    plt.subplot(2, 1, 2)
    plt.scatter(
        y_test_real,
        errors,
        s=MARKER_SIZE,
        facecolors="m",
        edgecolors="m",
        linewidths=4,
    )
    plt.title("Model Prediction Residual Errors", **TITLE_PLOT_KWARGS)
    plt.xlabel("True Molarity [M]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Molarity Difference [%]", **LABEL_PLOT_KWARGS)
    plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(
        results_path / "validation_pred_vs_true_molarities_and_errors.png"
    )
    plt.clf()


# Plot the Results for Battery Data Prediction:
def plot_battery_results(rate, voltage_data, y_pred_real, results_path):
    print("---> Plotting Predicted Molarities: LiPF6")
    times = pd.read_csv(f"{DATA_PATH}/battery/gitt_{rate}c_time.csv")
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    plt.subplot(3, 1, 1)
    plt.scatter(
        times["Time [min]"],
        y_pred_real,
        s=SMALLER_MARKER_SIZE,
        facecolors="m",
        edgecolors="m",
        linewidths=3,
        label="Pred-NN",
    )
    plt.title("Neural Network", **TITLE_PLOT_KWARGS)
    plt.xlabel("Time [min]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Molarity [M]", **LABEL_PLOT_KWARGS)
    plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])

    locs, labels = plt.yticks()

    # Calculate New Labels:
    min_max = [min(y_pred_real), max(y_pred_real)]
    increment = (min_max[1] - min_max[0]) / 6
    new_labels = np.linspace(min_max[0], min_max[1] + increment, 6)
    new_labels = [str(label) for label in np.around(new_labels, 3)]
    print(f"\nNew Labels: {new_labels}\n")

    # Calculate New Label Locations:
    min_max_loc = [min(locs), max(locs)]
    increment_loc = (min_max_loc[1] - min_max_loc[0]) / 6
    new_locs = np.linspace(min_max_loc[0], min_max_loc[1] + increment_loc, 6)
    new_locs = np.around(new_locs, 3)
    print(f"\nLabel Locs: {new_locs}\n")

    plt.yticks(new_locs, new_labels)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(3, 1, 2)
    pred_ratio = np.loadtxt(
        f"{DATA_PATH}/predictions/gitt_{rate}c_corrected.txt"
    )
    plt.scatter(
        times["Time [min]"],
        pred_ratio,
        s=SMALLER_MARKER_SIZE,
        facecolors="m",
        edgecolors="m",
        linewidths=3,
        label="Pred-Ratio",
    )
    plt.title("Ratio Technique", **TITLE_PLOT_KWARGS)
    plt.xlabel("Time [min]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Molarity [M]", **LABEL_PLOT_KWARGS)
    plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(3, 1, 3)
    min_max_times = [min(times["Time [min]"]), max(times["Time [min]"])]
    increment_time = (min_max_times[1] - min_max_times[0]) / len(voltage_data)
    new_times = np.linspace(
        min_max_times[0], min_max_times[1] + increment_time, len(voltage_data)
    )
    plt.plot(new_times, voltage_data["Voltage (mV)"], c="k", linewidth=4)
    plt.title("Voltage", **TITLE_PLOT_KWARGS)
    plt.xlabel("Time [min]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Voltage [mV]", **LABEL_PLOT_KWARGS)
    plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(
        results_path / f"pred_molarity_cnn_vs_ratio_with_voltage_{rate}c.png"
    )
    plt.clf()
