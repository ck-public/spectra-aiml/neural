import os
import random
import subprocess
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd
from joblib import load
from keras.models import load_model

# Local:
from neural.common.argument_parser import parse_arguments
from neural.common.electro_cnn import ElectroCNN
from neural.ionic.plotting import plot_battery_results, plot_true_vs_predicted
from neural.ionic.utils import (
    focus_dataset,
    preprocess_battery_data,
    preprocess_data,
    write_configuration,
)
from scipy.interpolate import interp1d
from sklearn.metrics import mean_squared_error, r2_score

# Globals:
REPO_PATH = (
    subprocess.check_output("git rev-parse --show-toplevel", shell=True)
    .decode()
    .replace("\n", "")
)
DATA_PATH = f"{REPO_PATH}/../data"


# ***************************************************************** #
# Model Training                                                    #
# ***************************************************************** #
def run_training(args):
    # Define the Spectra File and Create Results Path:
    print("\nDefining the Spectra File and Creating Results Path\n")
    timestamp = datetime.now().strftime("%m%d%Y-T-%H%M%S")
    spectra_file, results_path = "", ""
    if args.ion == "lithium":
        ratio = args.split.split("-")
        ec_split = ratio[0]
        emc_split = ratio[1]
        spectra_file = (
            f"{args.ion}/{args.measure}/"
            + f"spectra_ec_{ec_split}_emc_{emc_split}.csv"
        )
        results_path = (
            f"results/{args.ion}/{args.measure}/{args.split}/"
            + f"{args.restrict}_samples/{timestamp}"
        )
    else:
        spectra_file = f"{args.ion}/{args.measure}/spectra.csv"
        results_path = (
            f"results/{args.ion}/{args.measure}/"
            + f"{args.restrict}_samples/{timestamp}"
        )

    results_path = Path(results_path)
    results_path.mkdir(parents=True, exist_ok=True)

    # Write the Training Configuration to the Results Path:
    write_configuration(
        args, "Training", results_path, results_file="training_notes.txt"
    )

    # Handle Focusing the Dataset on Molarity or Wavenumber:
    if args.focus:
        base_spectra_file = str(spectra_file)
        keyword = "molarity" if args.molarity else "wavenumber"
        range_string = f"{args.min}-{args.max}"
        if args.wavenumber:
            range_string = f"{int(args.min)}-{int(args.max)}"

        focused_data_path = f"focused/{keyword}/{args.ion}/{range_string}"
        spectra_filename = spectra_file.split("/")[-1]
        spectra_file = f"{focused_data_path}/{spectra_filename}"

        # Make Sure Dataset File Exists:
        if not os.path.exists(f"{DATA_PATH}/{spectra_file}"):
            print(f"---> Spectra File Does NOT Exist ({spectra_file})")
            print(f"---> Creating Spectra File: {spectra_file}")
            focus_dataset(
                base_spectra_file,
                spectra_file,
                molarity=args.molarity,
                wavenumber=args.wavenumber,
                vrange=[args.min, args.max],
                write_file=True,
            )

    print("\n***** Configuration Files *****\n")
    print(f"---> Spectra File: {spectra_file}")

    # Train the CNN, Calculate Gradients, and Plot Results:
    train(spectra_file, args.restrict, results_path)


# Train CNN and Predict Molarities:
def train(spectra_file, restrict, results_path):
    # Splitting the Dataset:
    x_train, x_test, y_train, y_test, _, scaler_molar, _ = preprocess_data(
        spectra_file, restrict, results_path, plot_flag=True
    )

    # ***************************************************************** #
    # Model Training                                                    #
    # ***************************************************************** #
    # Build CNN:
    print("\nBuilding and Training the CNN\n")
    cnn = ElectroCNN(
        spectra_length=len(x_train[0]),
        results_path=results_path,
        conv_filters=2,
        conv_kernel_size=7,
        dense_activation="leaky_relu",
        output_filters=1,
        output_activation="linear",
    )
    cnn.fit(x_train, y_train)

    # ***************************************************************** #
    # Model Validation                                                  #
    # ***************************************************************** #
    # Get Predictions with Newly Trained Model:
    print("\nMaking Predictions with the CNN\n")
    y_pred = cnn.predict(x_test)
    r2 = r2_score(y_test, y_pred.reshape(-1))
    mse = mean_squared_error(y_test, y_pred.reshape(-1))
    var = np.var(np.abs(y_test - y_pred.reshape(-1)))
    print(f"---> Model Prediction R2 Score: {r2}")
    print(f"---> Model Prediction MSE: {mse}")
    print(f"---> Model Prediction Variance: {var}\n")

    # Transform Scaled Molarities Back to Molar Space:
    y_test_real = scaler_molar.inverse_transform(y_test.reshape(-1, 1)).reshape(
        -1
    )
    y_pred_real = scaler_molar.inverse_transform(y_pred.reshape(-1, 1)).reshape(
        -1
    )
    np.savetxt(results_path / "y_pred.txt", y_pred_real)
    np.savetxt(results_path / "y_test.txt", y_test_real)

    columns = ["Molarity (mol/L)"]
    true_values = pd.DataFrame(columns=columns)
    pred_values = pd.DataFrame(columns=columns)
    true_values[columns[0]] = y_test_real
    pred_values[columns[0]] = y_pred_real
    true_values.to_csv(results_path / "y_test_ionic.csv", index=False)
    pred_values.to_csv(results_path / "y_pred_ionic.csv", index=False)

    # Plot True vs Predicted Molarities and Errors:
    plot_true_vs_predicted(y_test_real, y_pred_real, r2, results_path)

    # Print Predicted Molaritys vs True Molarities:
    if len(y_test) < 5:
        indices = list(range(len(y_test)))
    else:
        indices = sorted(random.sample(list(range(len(y_test_real))), 5))
    for i in indices:
        print(f"---> Pred Molarity ({i}): {y_pred_real[i]}")
        print(f"---> True Molarity ({i}): {y_test_real[i]}\n")


# ***************************************************************** #
# Model Prediction                                                  #
# ***************************************************************** #
def run_prediction(args):
    if args.latest:
        base_results_path = f"results/{args.ion}/{args.measure}"
        if args.ion == "lithium":
            base_results_path = f"{base_results_path}/{args.split}"
        runs = subprocess.getoutput(f"ls -t {base_results_path}").split("\n")
        results_path = Path(f"{base_results_path}/{runs[0]}")
        args.results_path = results_path
    else:
        if not os.path.exists(args.results_path):
            raise RuntimeError(
                "Results path does not exist! Please supply a valid path."
            )
        results_path = Path(args.results_path)

    # Write the Prediction Configuration to Results Directory:
    write_configuration(
        args, "Prediction", results_path, results_file="prediction_notes.txt"
    )

    # Load the Trained Model for Prediction:
    print("\nLoading the Trained Model\n")
    model = load_model(results_path / "best_model.hdf5")
    model_config = model.get_config()
    input_shape = model_config["layers"][0]["config"]["batch_input_shape"]
    print(f"---> Expected Input Shape: {input_shape}\n")

    # Set the Battery Charging Options:
    charge_options = (
        [args.battery] if args.battery != "all" else ["1.00", "2.33", "3.00"]
    )

    std_devs = []
    for rate in charge_options:
        spectra_file = (
            f"battery/corrected/gitt_{rate}c.csv"
            if args.corrected
            else f"battery/gitt_{rate}c.csv"
        )

        # Load the Test Data and Scale:
        x_test, wavenumbers, _ = preprocess_battery_data(
            spectra_file,
            rate,
            args.focus,
            args.min,
            args.max,
            results_path=results_path,
            plot_flag=True,
        )
        print(f"---> Test Data Shape: {x_test.shape}")

        # Interpolate Data to Match Expected Input Shape:
        if not input_shape[1] == x_test.shape[1]:
            num_interp = input_shape[1]
            final_spectra = []
            for spectra in x_test:
                f1 = interp1d(wavenumbers, spectra.squeeze(), kind="cubic")
                new_wavenumbers = np.linspace(
                    wavenumbers.min(),
                    wavenumbers.max(),
                    num=num_interp,
                    endpoint=True,
                )
                new_spectra = f1(new_wavenumbers)
                final_spectra.append(np.expand_dims(new_spectra, axis=1))

            x_test = np.array(final_spectra)
        print(f"---> Final Test Data Shape: {x_test.shape}")

        if args.focus:
            value = 311
            if x_test.shape[-2] > value:
                x_test = x_test[:, :value, :]
                print(f"------> Updated Test Data Shape: {x_test.shape}")

        std_dev = predict(model, x_test, rate, results_path)
        std_devs.append(std_dev)

    detection_limit = len(std_devs) * np.mean(std_devs)
    print(f"Detection Limit: {detection_limit}")


def ratio_predict(x_test, wavenumbers):
    pred = [0.0] * len(x_test)
    wn1 = 1712
    wn2 = 1742
    wn1_idx = (np.abs(wavenumbers - wn1)).argmin()
    wn2_idx = (np.abs(wavenumbers - wn2)).argmin()
    for i, spectra in enumerate(x_test):
        pred[i] = (spectra[wn1_idx] / spectra[wn2_idx] - 0.0642) / 0.5905

    return pred


def predict(model, x_test, rate, results_path=""):
    # Get Predictions with Newly Trained Model:
    print(f"\nMaking Predictions with the CNN: {rate}C\n")
    y_pred = model.predict(x_test)

    # Transform the Predictions to Real Molarities:
    scaler_molar = load(results_path / "training_molar_scaler_model.bin")
    y_pred_real = scaler_molar.inverse_transform(y_pred.reshape(-1, 1)).reshape(
        -1
    )

    # Calculate the Depletion and Standard Deviation:
    depletion = y_pred_real.max() - y_pred_real.min()
    std_dev = np.std(y_pred_real[:10])
    print(f"------> Y Pred Data Shape: {y_pred_real.shape}")
    print(
        f"------> Y Pred Data Range: ({y_pred_real.min()}, {y_pred_real.max()})"
    )
    print(f"------> {rate}c Concentration Depletion: {depletion}")
    print(f"------> Y Pred Data StdDev: {std_dev}")
    np.savetxt(results_path / f"y_pred_battery_{rate}.txt", y_pred_real)

    # Plot True Molarities and Predicted Molarities:
    voltage_file = f"battery/gitt_{rate}c_voltage.csv"
    voltage_data = pd.read_csv(f"{DATA_PATH}/{voltage_file}")
    plot_battery_results(rate, voltage_data, y_pred_real, results_path)

    return std_dev


if __name__ == "__main__":
    args = parse_arguments()

    if args.train:
        run_training(args)

    # Make Predictions Using a Trained CNN:
    if args.predict:
        run_prediction(args)
    if args.train:
        run_training(args)

    # Make Predictions Using a Trained CNN:
    if args.predict:
        run_prediction(args)
