import os
import random
import subprocess
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd
from keras.models import load_model
from neural.common.argument_parser import parse_arguments
from neural.common.electro_cnn import ElectroCNN
from neural.system.plotting import plot_battery_results, plot_true_vs_predicted
from neural.system.utils import (
    focus_dataset_fractions,
    focus_dataset_spectra,
    preprocess_battery_data,
    preprocess_data,
    write_configuration,
)
from scipy.interpolate import interp1d
from sklearn.metrics import mean_squared_error, r2_score

# Globals:
REPO_PATH = (
    subprocess.check_output("git rev-parse --show-toplevel", shell=True)
    .decode()
    .replace("\n", "")
)
DATA_PATH = f"{REPO_PATH}/../data"


# ***************************************************************** #
# Model Training                                                    #
# ***************************************************************** #
def run_training(args):
    # Define the Spectra File and Create Results Path:
    print(
        "\nDefining the Spectra File, Fractions File, and Creating Results "
        "Path\n"
    )
    timestamp = datetime.now().strftime("%m%d%Y-T-%H%M%S")
    spectra_file, fractions_file, results_path = "", "", ""
    if args.ion == "lithium":
        ratio = args.split.split("-")
        ec_split = ratio[0]
        emc_split = ratio[1]
        spectra_file = f"{args.ion}/{args.measure}/"
        f"spectra_ec_{ec_split}_emc_{emc_split}.csv"
        fractions_file = f"{args.ion}/{args.measure}/"
        f"mole_fractions_ec_{ec_split}_emc_{emc_split}.csv"
        results_path = f"results/{args.ion}/{args.measure}/{args.split}/"
        f"{args.restrict}_samples/{timestamp}"
    else:
        spectra_file = f"{args.ion}/{args.measure}/spectra.csv"
        fractions_file = f"{args.ion}/{args.measure}/mole_fractions.csv"
        results_path = f"results/{args.ion}/{args.measure}/"
        f"{args.restrict}_samples/{timestamp}"

    results_path = Path(results_path)
    results_path.mkdir(parents=True, exist_ok=True)

    write_configuration(
        args, "Training", results_path, results_file="training_notes.txt"
    )

    # Handle Focusing the Dataset on Molarity or Wavenumber:
    if args.focus:
        base_spectra_file = str(spectra_file)
        base_fractions_file = str(fractions_file)
        keyword = "molarity" if args.molarity else "wavenumber"
        range_string = f"{args.min}-{args.max}"
        if args.wavenumber:
            range_string = f"{int(args.min)}-{int(args.max)}"

        focused_data_path = f"focused/{keyword}/{args.ion}/{range_string}"
        spectra_filename = spectra_file.split("/")[-1]
        spectra_file = f"{focused_data_path}/{spectra_filename}"

        if args.molarity:
            fractions_filename = fractions_file.split("/")[-1]
            fractions_file = f"{focused_data_path}/{fractions_filename}"

        # Make Sure Dataset File Exists:
        if not os.path.exists(f"{DATA_PATH}/{spectra_file}"):
            print(f"---> Spectra File Does NOT Exist ({spectra_file})")
            print(f"---> Creating Spectra File: {spectra_file}")
            focus_dataset_spectra(
                base_spectra_file,
                spectra_file,
                molarity=args.molarity,
                wavenumber=args.wavenumber,
                vrange=[args.min, args.max],
                write_file=True,
            )
        if not os.path.exists(f"{DATA_PATH}/{fractions_file}"):
            print(f"---> Fractions File Does NOT Exist ({fractions_file})")
            print(f"---> Creating Fractions File: {fractions_file}")
            focus_dataset_fractions(
                base_fractions_file,
                fractions_file,
                vrange=[args.min, args.max],
                write_file=True,
            )

    print("\n***** Configuration Files *****\n")
    print(f"---> Spectra File: {spectra_file}")
    print(f"---> Fractions File: {fractions_file}\n")

    train(spectra_file, fractions_file, args.restrict, results_path)


def train(spectra_file, fractions_file, restrict, results_path=""):
    # Splitting the Dataset:
    (
        x_train,
        x_test,
        y_train,
        y_test,
        _,
        _,
        comp_names,
        _,
        molarities,
    ) = preprocess_data(
        spectra_file, fractions_file, restrict, results_path, plot_flag=True
    )

    # ***************************************************************** #
    # Model Training                                                    #
    # ***************************************************************** #
    # Build CNN:
    print("\nBuilding and Training the CNN\n")
    cnn = ElectroCNN(
        spectra_length=len(x_train[0]),
        results_path=results_path,
        conv_filters=4,
        conv_kernel_size=9,
        dense_activation="leaky_relu",
        output_filters=len(comp_names),
        output_activation="linear",
    )
    cnn.fit(x_train, y_train)

    # ***************************************************************** #
    # Model Validation                                                  #
    # ***************************************************************** #
    # Get Predictions with Newly Trained Model:
    print("\nMaking Predictions with the CNN\n")
    y_pred = cnn.predict(x_test)
    print(f"---> Predictions Shape: {y_pred.shape}")
    r2s = [r2_score(y_test[:, i], y_pred[:, i]) for i in range(len(comp_names))]
    mses = [
        mean_squared_error(y_test[:, i], y_pred[:, i])
        for i in range(len(comp_names))
    ]
    vars = [
        np.var(np.abs(y_test[:, i] - y_pred[:, i]))
        for i in range(len(comp_names))
    ]
    np.savetxt(results_path / "y_pred.txt", y_pred)
    for i, comp_name in enumerate(comp_names):
        print(f"---> {comp_name} Model Prediction R2 Score: {r2s[i]}")
        print(f"---> {comp_name} Model Prediction MSE Score: {mses[i]}")
        print(f"---> {comp_name} Model Prediction Variance: {vars[i]}\n")

    # Save True and Predicted Values to File:
    columns = ["Molarity (mol/L)"] + [f"Mole % {comp}" for comp in comp_names]
    true_values = pd.DataFrame(columns=columns)
    pred_values = pd.DataFrame(columns=columns)
    true_values[columns[0]] = molarities
    true_values[columns[1:]] = y_test
    pred_values[columns[0]] = molarities
    pred_values[columns[1:]] = y_pred
    true_values.to_csv(results_path / "y_test_system.csv", index=False)
    pred_values.to_csv(results_path / "y_pred_system.csv", index=False)

    # Plot True vs Predicted Molarities and Errors:
    plot_true_vs_predicted(comp_names, y_test, y_pred, r2s, results_path)

    # Print Predicted vs True Mole Fractions:
    print("\nExample Predictions from the CNN\n")
    if len(y_test) < 5:
        indices = list(range(len(y_test)))
    else:
        indices = sorted(random.sample(list(range(len(y_test))), 5))
    for i in indices:
        print(f"---> Pred Mole Fractions ({i}): {y_pred[i]}")
        print(f"---> True Mole Fractions ({i}): {y_test[i]}\n")


# ***************************************************************** #
# Model Prediction                                                  #
# ***************************************************************** #
def run_prediction(args):
    if args.latest:
        base_results_path = f"results/{args.ion}/{args.measure}"
        if args.ion == "lithium":
            base_results_path = (
                f"{base_results_path}/{args.split}/{args.restrict}_samples"
            )
        else:
            base_results_path = f"{base_results_path}/{args.restrict}_samples"
        runs = subprocess.getoutput(f"ls -t {base_results_path}").split("\n")
        results_path = Path(f"{base_results_path}/{runs[0]}")
        args.results_path = results_path
    else:
        if not os.path.exists(args.results_path):
            raise RuntimeError(
                "Results path does not exist! Please supply a valid path."
            )
        results_path = Path(args.results_path)

    # Write the Prediction Configuration to Results Directory:
    write_configuration(
        args, "Prediction", results_path, results_file="prediction_notes.txt"
    )

    # Load the Trained Model for Prediction:
    print("\nLoading the Trained Model\n")
    model = load_model(results_path / "best_model.hdf5")
    model_config = model.get_config()
    input_shape = model_config["layers"][0]["config"]["batch_input_shape"]
    print(f"---> Expected Input Shape: {input_shape}\n")

    # Set the Battery Charging Options:
    charge_options = (
        [args.battery] if args.battery != "all" else ["1.00", "2.33", "3.00"]
    )

    std_devs = []
    comp_names = []
    for rate in charge_options:
        spectra_file = (
            f"battery/corrected/gitt_{rate}c.csv"
            if args.corrected
            else f"battery/gitt_{rate}c.csv"
        )

        # Load the Test Data and Scale:
        x_test, wavenumbers, _ = preprocess_battery_data(
            spectra_file,
            rate,
            args.focus,
            args.min,
            args.max,
            results_path=results_path,
            plot_flag=True,
        )
        print(f"---> Test Data Shape: {x_test.shape}")

        # Interpolate Data to Match Expected Input Shape:
        if not input_shape[1] == x_test.shape[1]:
            num_interp = input_shape[1]
            final_spectra = []
            for spectra in x_test:
                f1 = interp1d(wavenumbers, spectra.squeeze(), kind="cubic")
                new_wavenumbers = np.linspace(
                    wavenumbers.min(),
                    wavenumbers.max(),
                    num=num_interp,
                    endpoint=True,
                )
                new_spectra = f1(new_wavenumbers)
                final_spectra.append(np.expand_dims(new_spectra, axis=1))

            x_test = np.array(final_spectra)
        print(f"---> Final Test Data Shape: {x_test.shape}")

        std_dev, comp_names = predict(x_test, rate, results_path)
        std_devs.append(std_dev)

    detection_limits = len(std_devs) * np.mean(std_devs, axis=0)
    limits = dict(zip(comp_names, detection_limits))
    print(f"\nDetection Limits: {limits}\n")


def predict(x_test, rate, results_path=""):
    # Loading and Parsing the Model:
    model = load_model(results_path / "best_model.hdf5")

    # Loading Validation Predictions to Determine Components:
    val_preds = np.loadtxt(results_path / "y_pred.txt")
    comp_names = ["LiPF6", "EC", "EMC"]
    if 4 == val_preds.shape[1]:
        comp_names = ["Li", "PF6", "EC", "EMC"]
    elif "tegdme" in results_path.as_posix():
        comp_names = ["TEGDME", "LiTFSI"]

    # Get Predictions with Newly Trained Model:
    print(f"\nMaking Predictions with the CNN: {rate}C\n")
    y_pred = model.predict(x_test)

    depletion = np.max(y_pred, axis=0) - np.min(y_pred, axis=0)
    std_devs = [np.std(y_pred[:, i][:10]) for i in range(len(comp_names))]
    print(f"------> Y Pred Data Shape: {y_pred.shape}")
    print(
        f"------> Y Pred Data Range: ({np.min(y_pred, axis=0)}, "
        f"{np.max(y_pred, axis=0)})"
    )
    print(f"------> {rate}c Concentration Depletion: {depletion}")
    print(f"------> Y Pred Data StdDev: {std_devs}")
    np.savetxt(results_path / f"y_pred_battery_{rate}.txt", y_pred)

    # Plot True Molarities and Predicted Molarities:
    voltage_file = f"battery/gitt_{rate}c_voltage.csv"
    voltage_data = pd.read_csv(f"{DATA_PATH}/{voltage_file}")
    plot_battery_results(comp_names, rate, voltage_data, y_pred, results_path)

    return std_devs, comp_names


if __name__ == "__main__":
    args = parse_arguments()

    if args.train:
        run_training(args)

    # Make Predictions Using a Trained CNN:
    if args.predict:
        run_prediction(args)
