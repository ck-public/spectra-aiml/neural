import os
import random
import subprocess
from argparse import ArgumentParser
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from joblib import dump
from plotting import plot_battery_data, plot_data, plot_fractions_data
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

# Globals:
REPO_PATH = (
    subprocess.check_output("git rev-parse --show-toplevel", shell=True)
    .decode()
    .replace("\n", "")
)
DATA_PATH = f"{REPO_PATH}/../data"


TITLE_PLOT_KWARGS = {"fontsize": 60, "fontweight": "bold"}
LABEL_PLOT_KWARGS = {"fontsize": 50, "fontweight": "bold"}
LEGEND_PLOT_KWARGS = {"fontsize": 40}


def get_wn_column(dataset):
    wn_column = "Wavenumber (cm^-1)"
    if "Wavenumber" in dataset.columns:
        wn_column = "Wavenumber"

    return wn_column


# ***************************************************************** #
# Ratio Technique                                                   #
# ***************************************************************** #
def ratio(spectra_file):
    dataset = pd.read_csv(f"{DATA_PATH}/{spectra_file}")
    wn_column = get_wn_column(dataset)
    wavenumbers = dataset[wn_column].values
    spectras = np.array(
        [dataset[column].values for column in dataset.columns[1:]]
    )
    pred = [0.0] * len(spectras)
    wn1 = 1712
    wn2 = 1742
    wn1_idx = (np.abs(wavenumbers - wn1)).argmin()
    wn2_idx = (np.abs(wavenumbers - wn2)).argmin()
    for i, spectra in enumerate(spectras):
        pred[i] = (spectra[wn1_idx] / spectra[wn2_idx] - 0.0642) / 0.5905

    results = np.array(pred)
    filebase = spectra_file.split("/")[-1][:-4]
    if "corrected" in spectra_file:
        filebase = f"{filebase}_corrected"
    print(f"Filebase: {filebase}\n")
    np.savetxt(f"{DATA_PATH}/predictions/{filebase}.txt", results)

    indices = np.array(list(range(len(results))))
    plt.figure(figsize=(20, 10), dpi=400)
    plt.scatter(
        indices, results, s=40, facecolors="none", edgecolors="m", label="Pred"
    )
    plt.title("Ratio Predictions", fontsize=20, fontweight="bold")
    plt.xlabel("Index [-]", fontsize=15, fontweight="bold")
    plt.ylabel("Molarity [M]", fontsize=15, fontweight="bold")
    plt.tight_layout()
    plt.savefig(f"{DATA_PATH}/predictions/{filebase}.png")
    plt.clf()


# ***************************************************************** #
# Dataset Preprocessing                                             #
# ***************************************************************** #
def preprocess_battery_data(
    spectra_file,
    rate,
    focus=False,
    min=None,
    max=None,
    results_path="",
    plot_flag=False,
):
    # Load the Spectra Dataset:
    print(f"\n***** Loading Battery Dataset for {rate}c *****\n")
    print(f"---> Spectra File: {spectra_file}\n")
    dataset = pd.read_csv(f"{DATA_PATH}/{spectra_file}")

    print(f"---> Dataset Length: {len(dataset.columns[1:])}\n")
    print(dataset.describe())

    # Seperate the Dataset:
    print("\nSeperating the Dataset\n")
    wn_column = get_wn_column(dataset)
    wavenumbers = dataset[wn_column].values
    spectra = np.array(
        [dataset[column].values for column in dataset.columns[1:]]
    )
    # spectra = (spectra - np.min(spectra)) / (np.max(spectra) - np.min(spectra))
    print(f"---> Wavenumbers Shape: {wavenumbers.shape}")
    print(f"---> Spectra Shape: {spectra.shape}\n")

    # Scale Spectra:
    print("\nStandardizing the Spectra\n")
    scaler_spectra = StandardScaler()
    spectra_mod = scaler_spectra.fit_transform(spectra)
    dump(
        scaler_spectra,
        results_path / "battery_spectra_scaler_model.bin",
        compress=True,
    )
    print(f"---> Spectra Shape: {spectra.shape}")
    print(f"---> Scaled Spectra Shape: {spectra_mod.shape}")

    spectra_mod = spectra_mod.reshape(len(spectra_mod), -1, 1)

    # Plotting the Spectra:
    if plot_flag:
        plot_battery_data(rate, spectra, spectra_mod, wavenumbers, results_path)

    return spectra_mod, wavenumbers, scaler_spectra


# ***************************************************************** #
# Dataset Preprocessing                                             #
# ***************************************************************** #
def preprocess_data(
    spectra_file, fractions_file, restrict, results_path="", plot_flag=False
):
    # Load the Spectra and Fractions Dataset:
    print("\nLoading Spectra and Fractions Datasets\n")
    dataset = pd.read_csv(f"{DATA_PATH}/{spectra_file}")
    fractions_dataset = pd.read_csv(f"{DATA_PATH}/{fractions_file}")
    print(f"------> Dataset Length (Spectra): {len(dataset.columns[1:])}\n")
    print(f"------> Dataset Length (Fractions): {len(fractions_dataset)}\n")

    # Seperate the Dataset:
    print("\nSeperating the Dataset\n")
    wn_column = get_wn_column(dataset)
    wavenumbers = dataset[wn_column].values
    molarities = np.array(
        [float(column.replace("M", "")) for column in dataset.columns[1:]]
    )
    spectra = np.array(
        [dataset[column].values for column in dataset.columns[1:]]
    )

    # Formatting Mole Fractions:
    columns = ["Mole % LiPF6", "Mole % EC", "Mole % EMC"]
    if "tegdme" in spectra_file:
        columns = ["Mole % TEGDME", "Mole % LiTFSI"]
    elif "0-100" in results_path.as_posix():
        columns = ["Mole % LiPF6", "Mole % EMC"]

    comp_names = [column.split("%")[-1].strip() for column in columns]
    mole_fractions = fractions_dataset[columns].to_numpy()
    print(f"------> Wavenumbers Shape: {wavenumbers.shape}")
    print(f"------> Spectra Shape: {spectra.shape}")
    print(f"------> Mole Fractions Shape: {mole_fractions.shape}")
    print(
        f"---> Mole Fractions Range: ({np.min(mole_fractions, axis=0)}, "
        f"{np.max(mole_fractions, axis=0)})\n"
    )

    # Restrict the Data:
    samples = spectra.shape[0]
    if restrict is not None:
        print(f"---> Restricting the Dataset to {restrict} Samples")
        indices = random.sample(list(range(samples)), restrict)
        spectra = spectra[indices, :]
        mole_fractions = mole_fractions[indices, :]
        molarities = molarities[indices]
        print(f"------> Spectra Shape: {spectra.shape}")
        print(f"------> Mole Fractions Shape: {mole_fractions.shape}")

    # Scale Spectra:
    print("\nStandardizing the Spectra\n")
    scaler_spectra = StandardScaler()
    spectra_mod = scaler_spectra.fit_transform(spectra)
    dump(
        scaler_spectra,
        results_path / "training_spectra_scaler_model.bin",
        compress=True,
    )
    print(f"---> Spectra Shape: {spectra.shape}")
    print(f"---> Scaled Spectra Shape: {spectra_mod.shape}")

    # Splitting the Dataset:
    print("\nSplitting the Dataset into Train and Test\n")
    spectra_mod = spectra_mod.reshape(len(spectra_mod), -1, 1)
    (
        x_train,
        x_test,
        y_train,
        y_test,
        molarities_train,
        molarities_test,
    ) = train_test_split(
        spectra_mod,
        mole_fractions,
        molarities,
        test_size=0.2,
        random_state=42,
        shuffle=True,
    )
    print(f"---> Spectra Shape: {spectra_mod.shape}")
    print(f"---> X Train/Test Shape: {x_train.shape} - {x_test.shape}")
    print(f"---> Y Train/Test Shape: {y_train.shape} - {y_test.shape}")

    # Plotting the Spectra:
    if plot_flag:
        plot_data(
            spectra,
            spectra_mod,
            scaler_spectra,
            x_train,
            x_test,
            wavenumbers,
            results_path,
        )
        plot_fractions_data(
            molarities,
            molarities_train,
            molarities_test,
            columns,
            mole_fractions,
            y_train,
            y_test,
            results_path,
        )

    return (
        x_train,
        x_test,
        y_train,
        y_test,
        wavenumbers,
        scaler_spectra,
        comp_names,
        molarities_train,
        molarities_test,
    )


# ***************************************************************** #
# Focus Dataset by Molarity or Wavenumber                           #
# ***************************************************************** #
def focus_dataset_spectra(
    spectra_file,
    new_spectra_file,
    molarity=False,
    wavenumber=False,
    vrange=[],
    write_file=False,
):
    # Load the Spectra Dataset:
    print("\nLoading Dataset\n")
    focused_dataset_path = os.path.dirname(new_spectra_file)
    dataset = pd.read_csv(f"{DATA_PATH}/{spectra_file}")
    print(f"---> Dataset Length: {len(dataset.columns[1:])}\n")

    # Seperate the Dataset:
    print("\nSeperating the Dataset\n")
    wn_column = get_wn_column(dataset)
    wavenumbers = dataset[wn_column].values
    molarities = np.array(
        [float(column.replace("M", "")) for column in dataset.columns[1:]]
    )
    print(f"---> Wavenumber Count: {len(wavenumbers)}")
    print(f"---> Wavenumbers: {wavenumbers}")
    print(f"---> Molarities Count: {len(molarities)}")

    # Run the Focusing of the Dataset:
    if molarity:
        molar_matches = [
            f"{m}" for m in molarities if vrange[0] <= m <= vrange[1]
        ]
        print(f"---> Matched Molarities Count: {len(molar_matches)}")
        focused = dataset[[wn_column] + molar_matches]
    elif wavenumber:
        wn_matches = [w for w in wavenumbers if vrange[0] <= w <= vrange[1]]
        print(f"---> Matched Wavenumber Count: {len(wn_matches)}\n")
        focused = dataset.loc[dataset[wn_column].isin(wn_matches)]

    # Write the File for Use in Future Training Runs:
    if write_file:
        Path(f"{DATA_PATH}/{focused_dataset_path}").mkdir(
            parents=True, exist_ok=True
        )
        focused.to_csv(f"{DATA_PATH}/{new_spectra_file}", index=False)
        print(f"---> Wrote File: {new_spectra_file}\n")

    return focused


def focus_dataset_fractions(
    fractions_file, new_fractions_file, vrange=[], write_file=False
):
    # Load the Spectra Dataset:
    print("\nLoading Dataset\n")
    focused_dataset_path = os.path.dirname(new_fractions_file)
    dataset = pd.read_csv(f"{DATA_PATH}/{fractions_file}")
    print(f"---> Dataset Length: {len(dataset.columns[1:])}\n")

    # Seperate the Dataset:
    print("\nSeperating the Dataset\n")
    molarity_column = "Molarity (mol/L)"
    molarities = dataset[molarity_column].values
    print(f"---> Molarities Count: {len(molarities)}")

    # Run the Focusing of the Dataset:
    molar_matches = [m for m in molarities if vrange[0] <= m <= vrange[1]]
    print(f"---> Matched Molarities Count: {len(molar_matches)}")
    focused = dataset.loc[dataset[molarity_column].isin(molar_matches)]

    # Write the File for Use in Future Training Runs:
    if write_file:
        Path(f"{DATA_PATH}/{focused_dataset_path}").mkdir(
            parents=True, exist_ok=True
        )
        focused.to_csv(f"{DATA_PATH}/{new_fractions_file}", index=False)
        print(f"---> Wrote File: {new_fractions_file}\n")

    return focused


# ***************************************************************** #
# Write the Training Configuration                                  #
# ***************************************************************** #
def write_configuration(args, type, results_path, results_file):
    with open(f"{results_path}/{results_file}", "w+") as wf:
        wf.write(f"{type} Run {results_path}\n")
        wf.write("\nCommand: \n")
        wf.write("\n---> General Arguments\n")
        wf.write(f"------>> Ion Type: {args.ion}\n")
        wf.write("\n---> Focus Arguments\n")
        wf.write(f"------>> Focus Dataset: {args.focus}\n")
        wf.write(f"------>> Focus by Molarity: {args.molarity}\n")
        wf.write(f"------>> Focus by Wavenumber: {args.wavenumber}\n")
        wf.write(f"------>> Focus Range Min: {args.min}\n")
        wf.write(f"------>> Focus Range Max: {args.max}\n")
        wf.write("\n---> Training Arguments\n")
        wf.write(f"------>> Run Training: {args.train}\n")
        wf.write(f"------>> Dataset Type: {args.measure}\n")
        wf.write("\n---> Prediction Arguments\n")
        wf.write(f"------>> Run Prediction: {args.predict}\n")
        wf.write(f"------>> Use Latest Training Run: {args.latest}\n")
        wf.write(f"------>> Use Baseline Corrected Data: {args.corrected}\n")
        wf.write(f"------>> Battery Rate for Prediction: {args.battery}\n")
        wf.write(f"------>> Results Path: {args.results_path}\n\n")


if __name__ == "__main__":
    parser = ArgumentParser("Focus a Dataset on Molarity or Wavenumber")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--molarity", action="store_true", help="Focus Dataset on Molarity"
    )
    group.add_argument(
        "--wavenumber", action="store_true", help="Focus Dataset on Wavenumber"
    )
    parser.add_argument(
        "--min",
        required=True,
        type=float,
        default=0.0,
        help="Set the Range Minimum to Focus On",
    )
    parser.add_argument(
        "--max",
        required=True,
        type=float,
        default=0.0,
        help="Set the Range Maximum to Focus On",
    )
    parser.add_argument(
        "--ion",
        type=str,
        default="lithium",
        choices=["lithium", "tegdme"],
        help="Electrolyte System to Model",
    )
    parser.add_argument(
        "--measure",
        type=str,
        default="sim",
        choices=["sim", "msr"],
        help="Use the Measured (msr) or Simulated (sim) Dataset",
    )

    args = parser.parse_args()
    print(f"\nArguments: {args}\n")

    spectra_file = f"electrolyte_data_{args.measure}_{args.ion}.csv"
    fractions_file = f"mole_fractions_{args.measure}_{args.ion}.csv"

    preprocess_data(
        spectra_file, fractions_file, results_path=Path("test"), plot_flag=True
    )
