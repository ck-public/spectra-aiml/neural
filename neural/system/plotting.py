import subprocess

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Globals:
REPO_PATH = (
    subprocess.check_output("git rev-parse --show-toplevel", shell=True)
    .decode()
    .replace("\n", "")
)
DATA_PATH = f"{REPO_PATH}/../data"

# Plotting Globals:
DPI = 80
FIGURE_SIZE = (40, 30)
TITLE_PLOT_KWARGS = {"fontsize": 90, "fontweight": "bold"}
LABEL_PLOT_KWARGS = {"fontsize": 80, "fontweight": "bold"}
LEGEND_PLOT_KWARGS = {"fontsize": 80}
MARKER_SIZE = 1000
SMALLER_MARKER_SIZE = 800


# Plot Training Data Spectra:
def plot_data(
    spectra,
    spectra_mod,
    scaler_spectra,
    x_train,
    x_test,
    wavenumbers,
    results_path,
):
    # Plot All Spectra (Train and Test):
    print("\nPlotting the Spectra\n")
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    plt.subplot(2, 1, 1)
    for s in spectra:
        plt.plot(wavenumbers, s)
    plt.title("All Spectra (Original)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(2, 1, 2)
    for s in spectra_mod:
        plt.plot(wavenumbers, s)
    plt.title("All Spectra (Scaled)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / "spectra_all.png")
    plt.clf()

    # Plot Training Spectra Only:
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    x_train_original = scaler_spectra.inverse_transform(x_train.squeeze())
    plt.subplot(2, 1, 1)
    for s in x_train_original:
        plt.plot(wavenumbers, s)
    plt.title("Training Spectra (Original)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(2, 1, 2)
    for s in x_train:
        plt.plot(wavenumbers, s)
    plt.title("Training Spectra (Spectra)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / "spectra_train.png")
    plt.clf()

    # Plot Testing Spectra Only:
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    x_test_original = scaler_spectra.inverse_transform(x_test.squeeze())
    plt.subplot(2, 1, 1)
    for s in x_test_original:
        plt.plot(wavenumbers, s)
    plt.title("Testing Spectra (Original)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(2, 1, 2)
    for s in x_test:
        plt.plot(wavenumbers, s)
    plt.title("Testing Spectra (Scaled)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / "spectra_test.png")
    plt.clf()


# Plot Training Data Spectra:
def plot_fractions_data(
    molarities,
    molarities_train,
    molarities_test,
    columns,
    mole_fractions,
    y_train,
    y_test,
    results_path,
):
    print("\nPlotting the Mole Fractions Data\n")
    comp_names = [column.split("%")[-1].strip() for column in columns]
    for i in range(len(columns)):
        comp_name = comp_names[i]
        print(f"---> Plotting {comp_name}")

        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        plt.scatter(
            molarities,
            mole_fractions[:, i],
            s=MARKER_SIZE,
            facecolors="m",
            edgecolors="m",
            label=comp_names[i],
        )
        plt.title(f"Mole Fractions {comp_name} (Original)", **TITLE_PLOT_KWARGS)
        plt.xlabel("Molarity [mol/L]", **LABEL_PLOT_KWARGS)
        plt.ylabel("Mole Fraction [-]", **LABEL_PLOT_KWARGS)
        plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        for spine in plt.gca().spines.values():
            spine.set_visible(False)

        plt.tight_layout()
        plt.savefig(results_path / f"mole_fractions_{comp_name.lower()}.png")
        plt.clf()

    # Plot Training Mole Fractions Only:
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    for i in range(len(columns)):
        plt.scatter(
            molarities_train, y_train[:, i], s=MARKER_SIZE, label=comp_names[i]
        )
    plt.title("Training Mole Fractions", **TITLE_PLOT_KWARGS)
    plt.xlabel("Molarity [mol/L]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Mole Fraction [-]", **LABEL_PLOT_KWARGS)
    plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.legend(**LEGEND_PLOT_KWARGS)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / "mole_fractions_train.png")
    plt.clf()

    # Plot Testing Mole Fractions Only:
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    for i in range(len(columns)):
        plt.scatter(
            molarities_test, y_test[:, i], s=MARKER_SIZE, label=comp_names[i]
        )
    plt.title("Testing Mole Fractions (Original)", **TITLE_PLOT_KWARGS)
    plt.xlabel("Molarity [mol/L]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Mole Fraction [-]", **LABEL_PLOT_KWARGS)
    plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
    plt.legend(**LEGEND_PLOT_KWARGS)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / "mole_fractions_test.png")
    plt.clf()


# Plot Battery Cycling Data Spectra:
def plot_battery_data(rate, spectra, spectra_mod, wavenumbers, results_path):
    print("\nPlotting the Battery Cycling Spectra\n")
    plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
    plt.subplot(2, 1, 1)
    for s in spectra:
        plt.plot(wavenumbers, s)
    plt.title("Original Spectra", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.subplot(2, 1, 2)
    for s in spectra_mod:
        plt.plot(wavenumbers, s)
    plt.title("Scaled Spectra", **TITLE_PLOT_KWARGS)
    plt.xlabel("Wavenumber [cm^-1]", **LABEL_PLOT_KWARGS)
    plt.ylabel("Absorption [-]", **LABEL_PLOT_KWARGS)
    plt.xticks([])
    plt.yticks([])
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.tight_layout()
    plt.savefig(results_path / f"battery_spectra_{rate}.png")
    plt.clf()


# Plot True versus Predicted Molarities:
def plot_true_vs_predicted(
    comp_names, y_test_real, y_pred_real, scores, results_path
):
    for i in range(len(comp_names)):
        comp_name = comp_names[i]
        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        plt.subplot(2, 1, 1)
        plt.scatter(
            y_test_real[:, i],
            y_pred_real[:, i],
            s=MARKER_SIZE,
            facecolors="m",
            edgecolors="m",
            linewidths=4,
            label="Pred vs True",
        )
        plt.plot(
            y_test_real[:, i],
            y_test_real[:, i],
            c="k",
            linewidth=2,
            label="True vs True",
        )
        plt.annotate(
            f"Score: {round(scores[i], 6)}",
            xy=(0.60, 0.20),
            xycoords="axes fraction",
            fontsize=LABEL_PLOT_KWARGS["fontsize"],
        )
        plt.title(
            f"Model Predictions vs True Mole Fractions {comp_name}",
            **TITLE_PLOT_KWARGS,
        )
        plt.xlabel("True Mole Fraction [-]", **LABEL_PLOT_KWARGS)
        plt.ylabel("Pred Mole Fraction [-]", **LABEL_PLOT_KWARGS)
        plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        plt.legend(**LEGEND_PLOT_KWARGS)
        for spine in plt.gca().spines.values():
            spine.set_visible(False)

        # Plot the Error Residuals for Predictions:
        errors = (
            np.abs((y_test_real[:, i] - y_pred_real[:, i]) / y_test_real[:, i])
            * 100
        )
        plt.subplot(2, 1, 2)
        plt.scatter(
            y_test_real[:, i],
            errors,
            s=MARKER_SIZE,
            facecolors="m",
            edgecolors="m",
            linewidths=4,
        )
        plt.title(
            f"Model Prediction Residual Errors {comp_name}", **TITLE_PLOT_KWARGS
        )
        plt.xlabel("True Mole Fraction [-]", **LABEL_PLOT_KWARGS)
        plt.ylabel("Mole Fraction Difference [%]", **LABEL_PLOT_KWARGS)
        plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        for spine in plt.gca().spines.values():
            spine.set_visible(False)

        plt.tight_layout()
        plt.savefig(
            results_path / "validation_pred_vs_true_mole_fractions_and_errors_"
            f"{comp_name.lower()}.png"
        )
        plt.clf()


# Plot the Results for Battery Data Prediction:
def plot_battery_results(
    comp_names, rate, voltage_data, y_pred_real, results_path
):
    print(f"---> Plotting Predicted Mole Fractions: {comp_names}")
    times = pd.read_csv(f"{DATA_PATH}/battery/gitt_{rate}c_time.csv")
    for i in range(len(comp_names)):
        comp_name = comp_names[i]

        plt.figure(figsize=FIGURE_SIZE, dpi=DPI)
        plt.subplot(2, 1, 1)
        plt.scatter(
            times["Time [min]"],
            y_pred_real[:, i],
            s=SMALLER_MARKER_SIZE,
            facecolors="m",
            edgecolors="m",
            linewidths=3,
            label="Pred-NN",
        )
        plt.title("Neural Network", **TITLE_PLOT_KWARGS)
        plt.xlabel("Time [min]", **LABEL_PLOT_KWARGS)
        plt.ylabel("Mole Fraction [-]", **LABEL_PLOT_KWARGS)
        plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        for spine in plt.gca().spines.values():
            spine.set_visible(False)

        plt.subplot(2, 1, 2)
        min_max_times = [min(times["Time [min]"]), max(times["Time [min]"])]
        increment_time = (min_max_times[1] - min_max_times[0]) / len(
            voltage_data
        )
        new_times = np.linspace(
            min_max_times[0],
            min_max_times[1] + increment_time,
            len(voltage_data),
        )
        plt.plot(new_times, voltage_data["Voltage (mV)"], c="k", linewidth=4)
        plt.title("Voltage", **TITLE_PLOT_KWARGS)
        plt.xlabel("Time [min]", **LABEL_PLOT_KWARGS)
        plt.ylabel("Voltage [mV]", **LABEL_PLOT_KWARGS)
        plt.xticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        plt.yticks(fontsize=LABEL_PLOT_KWARGS["fontsize"])
        for spine in plt.gca().spines.values():
            spine.set_visible(False)

        plt.tight_layout()
        plt.savefig(
            results_path / f"pred_mole_fraction_cnn_with_voltage_{rate}c_"
            f"{comp_name.lower()}.png"
        )
        plt.clf()
