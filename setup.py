from setuptools import find_packages, setup

setup(
    name="spectra-neural",
    version="1.0",
    description="A repository containing neural network tools to analyze measured absorption spectra for various lithium ion salt concentration solutions.",
    author="CK",
    author_email="cobkind@gmail.com",
    package_dir={".": "."},
    packages=find_packages(),
    python_requires=">=3.6, <4",
    install_requires=[
        "keras",
        "kneed",
        "matplotlib",
        "numpy",
        "pandas",
        "scikit-learn",
        "scipy",
        "tensorflow; platform_system!='Darwin'",
        "tensorflow-macos; platform_system=='Darwin'",
        "tensorflow-metal; platform_system=='Darwin'",
    ],
)
