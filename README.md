# Neural

A repository containing neural network AI/ML tools to analyze measured
absorption spectra for various lithium ion salt concentration solutions.

[[_TOC_]]

# Getting Started

To get started on running the code in this repository, first make sure you have
you environment setup as specified in the `ENVIRONMENT.md` file in this
repository. In your terminal, run the following commands to create a new virtual
environment and install the dependencies required to run the code in the
repository.

**Note**: The install usually goes the most smoothly on `Git Bash`, so I would
recommend installing that before starting.

```bash
# Change directory to the repsository (if not already there).
cd neural

# Create a new virtual python environment.
python3 -m venv neural-venv

# Activate the virtual environment.
source neural-venv/bin/activate  # Mac-OS
source neural-venv/Scripts/activate  # Windows

# Always good practice to keep pip up to date.
python3 -m pip install --upgrade pip

# Install the required python libraries specified in the setup.py.
python3 -m pip install .
```

**Note**: On Windows, it may be necessary to enable long paths. Please follow
the Powershell section in the guide
[here](https://learn.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation?tabs=powershell#enable-long-paths-in-windows-10-version-1607-and-later)
to do this.

Having completed the commands above, you can now move on to running the code in
the repository.

# Code Execution

At this point, life is easy. You have everything set up and can now run the
respective code in your `Terminal`. This repository contains two model training
pipelines for the battery data:

1. `neural/ionic` - This is the single-component model training pipeline. This
   pipeline is dedicated towards training, predicting, and plotting the lithium
   ion (LiPF6) concentration from the datasets.
2. `neural/system` - This is the multi-component model training pipeline. This
   pipeline is dedicated towards training, predicting, and plotting the mole
   fractions of EC, EMC, and LiPF6 in the datasets.

Both the `ionic` and `system` folders contain a `run_cnn.py` script which will
be the primary interaction point for training and prediction. Both scripts
utilize the comment `argument_parser.py` and `electro_cnn.py` files from the
`neural/common` folder.

## Running the CNN

In order to run and train a model, change the directory to the model training
pipeline you would like to run (i.e. `cd neural/ionic` or `cd neural/system`).
Once you are in the desired directory, run the `run_cnn.py` script with the
`--help` command to learn more about the available arguments and what they do.

```bash
python3 run_cnn.py --help  # Print Off Command Line Arguments to Configure Clustering
```

The `help` command should log some output to the console looking like what is
below.

```bash
~ python3 run_cnn.py --help
usage: Run CNN on Given Spectra and Concentration Data [-h] [-i {lithium,tegdme}]
                                                       [--restrict {None,7,10,15,20,30,50,100,250,500,750,1000}] [-f] [--molarity]
                                                       [--wavenumber] [--min MIN] [--max MAX] [-t] [-m {interpolated,measured}]
                                                       [-s {0-100,20-80,30-70,50-50,70-30,80-20}] [-p] [-l] [-c]
                                                       [-b {1.00,2.33,3.00,all}] [-r RESULTS_PATH]

general:
  -i {lithium,tegdme}, --ion {lithium,tegdme}
                        The electrolyte system to model.
  --restrict {None,7,10,15,20,30,50,100,250,500,750,1000}
                        Set the maximum number of samples to train on.

focus:
  -f, --focus           Train on a dataset focused on wavenumber or molarity.
  --molarity            Create and train on a focused dataset on molarity.
  --wavenumber          Create and train on a focused dataset on wavenumber.
  --min MIN             Set the range minimum to focus the dataset on.
  --max MAX             Set the range maximum to focus the dataset on.

training:
  -t, --train           Train a new model on a dataset.
  -m {interpolated,measured}, --measure {interpolated,measured}
                        Train the model using the measured or interpolated dataset.
  -s {0-100,20-80,30-70,50-50,70-30,80-20}, --split {0-100,20-80,30-70,50-50,70-30,80-20}
                        The ratios of EC and EMC in the dataset. This only applies to lithium.

prediction:
  -p, --predict         Generate model predictions with a trained model on a battery dataset.
  -l, --latest          Run model predictions with the model from the latest training run.
  -c, --corrected       Run model predictions on the battery data corrected for baseline shifting.
  -b {1.00,2.33,3.00,all}, --battery {1.00,2.33,3.00,all}
                        The charging rate for the battery cycling data for model predictions.
  -r RESULTS_PATH, --results-path RESULTS_PATH
                        Path to run results folder for generating model predictions. This is required if --latest is not supplied.
```

## Model Training

Upon running the `run_cnn.py` script for the very first time, a folder called
`results` will be created. Inside of the `results` folder, another folder will
be created with the `--ion` argument specified (i.e. `results/lithium` or
`results/tegdme`). The `ion` folder will contain another folder with the
`--measure` argument specified (i.e. `results/lithium/interpolated` or
`results/lithium/measured`).

If training on the lithium ion dataset, the `measure` folder will contain another folder
with the EC-EMC split (only if the `lithium` ion was specified) (i.e.
`results/lithium/interpolated/50-50`), and finally, this folder will be divided
by the `--restrict` argument supplied (if any, i.e.
`results/lithium/interpolated/50-50/1000_samples`).

The research conducted forthis paper involved an in-depth model sensitivity and
overfitting analysis which is what drove this level of folder abtraction for the
training and prediction results.

Finally, the actual results for the current training run are stored in a folder
named from the timestamp of the initial execution of the `run_cnn.py` script.
The folder name follows the format `MMDDYYYY-T-HHMMSS`.

### Default Training

To run training on the full training dataset and the default configuration (30%
EC, 70% EMC), simply run the following command.

```bash
python3 run_cnn.py --train
```

### EC-EMC Split Training

To train on a specific split of EC-EMC, add the `--split` argument to the
command above. For example, to train on a 50/50 split of EC-EMC, run the
following command.

**Note**: As specified in the `data` repository `README.md`, this command
depends directly on the filenames of the datasets in the `data` repository.
Please make sure you follow the file naming convention outlined there to ensure
everything works correctly.

```bash
python3 run_cnn.py --train --split 5050
```

### Focused Dataset Training

The `run_cnn.py` script has the ability to allow you to focus a training dataset
either on a molarity range or by a wavenumber range. In order to run this
training, you must include the `--focus` argument, as well as the focus
parameter you would like to use (`--molarity` or `--wavenumber`), and finally,
the range of values you would like to focus the dataset between. Examples of
each focus parameter are shown below.

**Note**: When you run training with the `--focus` command, the focused dataset
created during the training run will be saved to the data repository. This way,
if you need to run the same training again, the dataset will be loaded directly
from file and won't need to be recreated from scratch.

```bash
# Focus the dataset and training on molarities between 0.75 and 2.75.
python3 run_cnn.py --train --focus --molarity --min 0.75 --max 2.75

# Focus the dataset and training on wavenumbers between 1000 and 2000.
python3 run_cnn.py --train --focus --wavenumber --min 1000 --max 2000
```

## Model Prediction

Once you have run the model training, the best trained model will be saved to a file
for use in prediction. The model is saved to the results folder from the
training run and is the restored weights from the best training epoch after all
model training callbacks have completed.

Model prediction will generate multiple new files in the results folder.

### Default Prediction

To generate predictions using the trained model on all original battery datasets
(i.e. uncorrected for baseline shifting) using the latest training run model, 
simply run the following command.

```bash
python3 run_cnn.py --predict --latest
```

### Prediction on a Specific Rate

To generate predictions on a specific set of battery data, use the `--battery`
argument, as shown below. This will only generate predictions for the specified
battery rate instead of all of the battery rates.

```bash
python3 run_cnn.py --predict --latest --battery 2.33
```

### Prediction with a Different Model

If for whatever reason you would like to use a different model for predictions
on the battery data, you must specify the path to the results folder. An example
of this command is below.

```bash
python3 run_cnn.py --predict --results-path path/to/the/results/you/want/to/use
```

### Prediction on Corrected Data

To generate predictions on the battery data corrected for baseline shifting,
simply add the `--corrected` argument, as shown below.

```bash
python3 run_cnn.py --predict --latest --corrected
```