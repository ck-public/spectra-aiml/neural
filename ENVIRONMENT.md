# Environment

This wiki contains a guide for getting setup for development and running the
code in this repository, as well as a quick `git` tutorial.

# Folder Setup

Determine where you would like the code to live on your machine. This can be something like `/Users/<your_username>/code` on `Mac` and `Linux` or `C:\Users\<your_username>\code` on `Windows`.

In your `Terminal`, `Command Prompt`, `Anaconda Prompt`, whatever you use, run the following commands to get into your folder and `clone` this repository.

```bash
cd /Users/<your_username>/code    # Mac or Linux
cd C:\Users\<your_username>\code  # Windows
git clone https://gitlab.com/ck-public/spectra-aiml/neural.git
cd neural
```

You now have the code cloned on your machine and have your `Terminal`, `Command Prompt`, `Anaconda Prompt`, whatever you use, pointed to the correct directory where the code lives.

# Development Setup

The best way to work with code is by using an IDE. I have found **VS Code** is a fantastic IDE and it supports a variety of langauges. If you are only concerned with Python, then PyCharm is phenomenal as well. I have linked the download URL's for these below (they are both free :)).

1. [VS Code](https://code.visualstudio.com/download)
2. [PyCharm](https://www.jetbrains.com/pycharm/download/#section=windows)

Once you have your IDE downloaded and opened, do the following:

1. Click `File`
2. Click `Open Folder`
3. Navigate to `/Users/<your_username>/code`
4. Click `Open`

This will now set your IDE to open the `code` folder containing all of the code. You can open, view, and change files all directly through this window. You can even open an integrated Terminal by clicking `Terminal` --> `New Terminal` and it will open right to the `code` directory you are working in. 

# Python Setup
Start by running the following command in your `Terminal`, `Command Prompt`, `Anaconda Prompt`, whatever you use, to check your version of Python. This project requires that you have at least `Python 3.6`.

```bash
python --version
python3 --version
```

Hopefully one of those will print `Python 3.6` or greater. If it doesn't, you will need to install a newer version of Python. For this, I recomment utilizing one of the following tools:

1. [PyEnv](https://github.com/pyenv/pyenv) - This is a very simple Python version manager. This is great for Mac or Linux.
2. [Anaconda](https://docs.anaconda.com/anaconda/install/index.html) - I use Anaconda on my Windows PC and it works relatively well. As good as you can hope with windows...

Once you have verified you are using at least `Python 3.6`, run the following commands to configure your environment and ensure the correct Python libraries are installed.

```bash
cd <repository>                       # Change directory to the repsository.
python3 -m venv my-venv               # Create a virtual python environment.
source my-venv/bin/activate           # Activate the virtual environment.
python3 -m pip install --upgrade pip  # Always good practice to keep pip up to date.
python3 -m pip install .              # Install the required python libraries specified in the setup.py.
```

# Git Tutorial

In this section I will give you a quick tutorial on how to use `Git` with code repositories. `Git` is a tool that allows configuration management for a code repository, basically ensuring that the main code is protected from other developers. There is ultimately **one (1)** primary `branch` and it is called `main`. This houses the stable and working code. Developers can then `branch` off of main without impacting the main code (think of it like a tree). If the developer would like to contribute something to the main code, then they can submit a `merge request` for their `branch` and other project members can review this `merge request`. Reviewers can either approve it and merge it in to the `main` code, or provide suggestions to fix and then approve and merge it once the suggestions have been fixed.

Now, `Git` is a local tool and provides no way to easily share a code repository between multiple people. This is where `GitHub` comes in. `GitHub` is an online sort of "database" where code is stored remotely and can be shared with anyone (as long as they have the correct access rights). This allows people to `pull` code down, make changes, and `push` those changes back up to the remote repository. Please start by creating an account on `GitHub` via the link below (this is also free). This will allow me to share our `Private` repository with you so you can access the code.

[GitHub](https://github.com/)

Now that you have a `GitHub` account, let me know and I can add you to the repo via your email.

There are a few `Git` commands to familiarize yourself with, but I promise they are simple.

### Pull Code from the Remote Repository

```bash
git pull  # Pull Code Down from the Remote Repository in GitHub
```

### Create a New Branch

```bash
git checkout -b <desired_branch_name>                 # Creates a New Branch Locally
git push --set-upstream origin <desired_branch_name>  # Pushes the New Branch Up to the Remote Repository
```

### Making Changes and Committing Them

Within your IDE you can make changes to files and save them, and `Git` will automatically detect that these files have been changed. The commands below will show you how to stage the changes and commit them.

```bash
git status                              # This Will Show You All of the Files Changed in Red
git add <filename>                      # This Will Add One File to the Staging Area
git add --all                           # Instead of Adding One File at a Time You Can Just Add them All
git status                              # You Should Now See the Red Changed Files Above are Now Green
git commit -m "<what did you change?>"  # Commits the Changes and Stores the File Diffs
```

### Push Changes to the Remote Repository

```bash
git push  # Push Code Up to the Remote Repository in GitLab
```